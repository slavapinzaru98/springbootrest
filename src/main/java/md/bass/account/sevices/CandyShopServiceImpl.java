package md.bass.account.sevices;

import md.bass.account.entities.CandyShop;
import md.bass.account.repositories.CandyShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandyShopServiceImpl implements CandyShopService{
    @Autowired
    CandyShopRepository candyShopRepository;
    @Override
    public Iterable<CandyShop> findAllCandyShop() {
        return candyShopRepository.findAll();
    }
    @Override
    public void deleteCandyShop(Integer id) {
        candyShopRepository.deleteById(id);
    }
    @Override
    public void saveCandyShop(CandyShop candyShop) {
        candyShopRepository.save(candyShop);
    }
}
