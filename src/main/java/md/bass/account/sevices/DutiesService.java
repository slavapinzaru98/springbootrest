package md.bass.account.sevices;

import md.bass.account.entities.Duties;

public interface DutiesService {
    Iterable<Duties> findDuties();
    void deleteDuties(Integer id);
    void saveDuties(Duties Duties);
}
