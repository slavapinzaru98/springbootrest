package md.bass.account.sevices;

import md.bass.account.entities.Duties;
import md.bass.account.repositories.DutiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DutiesServiceImpl implements DutiesService {
    @Autowired
    DutiesRepository dutiesRepository;
    @Override
    public Iterable<Duties> findDuties() {
        return dutiesRepository.findAll();
    }

    @Override
    public void deleteDuties(Integer id) {
        dutiesRepository.deleteById(id);
    }

    @Override
    public void saveDuties(Duties duties) {
        dutiesRepository.save(duties);
    }
}
