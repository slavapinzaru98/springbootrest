package md.bass.account.sevices;

import md.bass.account.entities.Account;
import java.util.Optional;

public interface AccountService {
    void updateAccount(Account account);
    Iterable<Account> findAccount();
    void deleteAccount(Integer id);
    void saveAccount(Account account);
    Optional<Account> findCertainAccount(Integer id);
}
