package md.bass.account.sevices;

import md.bass.account.entities.Employee;

public interface EmployeeService {
    Iterable<Employee> findEmployee();
    void deleteEmployee(Integer id);
    void saveEmployee(Employee employee);
}
