package md.bass.account.sevices;

import md.bass.account.entities.Purchase;


public interface PurchaseService {
    Iterable<Purchase> findAllPurchase();
    void  deletePurchase(Integer id);
    void savePurchase(Purchase purchase);
}