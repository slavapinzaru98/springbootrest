package md.bass.account.sevices;

import md.bass.account.entities.CandyShop;


public interface CandyShopService {
    Iterable<CandyShop> findAllCandyShop();
    void  deleteCandyShop(Integer id);
    void saveCandyShop(CandyShop candyShop);
}
