package md.bass.account.sevices;

import md.bass.account.entities.Account;
import md.bass.account.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    AccountRepository accountRepository;
    @Override
    public void updateAccount(Account account) {
        Optional<Account> oldAccount = accountRepository.findById(account.getId());
    }
    @Override
    public Iterable<Account> findAccount() { return accountRepository.findAll(); }
    @Override//try instead Optional iterable
    public Optional<Account> findCertainAccount(Integer id) {
        return accountRepository.findById(id);
    }

    @Override
    public void deleteAccount(Integer id) {
        accountRepository.deleteById(id);
    }
    @Override
    public void saveAccount(Account account) {
        accountRepository.save(account);
    }
}






//    @Override
//    public void findCertainAccount(Integer id) {
//        Optional<account> certainAccount = accountRepository.findById(id);
//    }