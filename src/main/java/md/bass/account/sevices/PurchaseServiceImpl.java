package md.bass.account.sevices;

import md.bass.account.entities.Purchase;
import md.bass.account.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseServiceImpl implements PurchaseService{
    @Autowired
    PurchaseRepository purchaseRepository;
    @Override
    public Iterable<Purchase> findAllPurchase() { return purchaseRepository.findAll(); }
    @Override
    public void deletePurchase(Integer id) {
    purchaseRepository.deleteById(id);
    }
    @Override
    public void savePurchase(Purchase purchase) {
        purchaseRepository.save(purchase);
    }


}
