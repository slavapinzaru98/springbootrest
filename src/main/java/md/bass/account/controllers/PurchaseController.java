package md.bass.account.controllers;


import md.bass.account.entities.Purchase;
import md.bass.account.sevices.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @RequestMapping(value = "/purchase" ,method = RequestMethod.GET)
    Iterable<Purchase> findAllPurchase(){return purchaseService.findAllPurchase();}
    @RequestMapping(value = "/purchase" ,method = RequestMethod.POST)
    void savePurchase(@RequestBody Purchase purchase){purchaseService.savePurchase(purchase);}
    @RequestMapping(value = "/purchase/{id}" ,method = RequestMethod.DELETE)
    void deletePurchase(@PathVariable Integer id){purchaseService.deletePurchase(id);}
}
