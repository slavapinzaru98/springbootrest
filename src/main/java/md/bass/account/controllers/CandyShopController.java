package md.bass.account.controllers;

import md.bass.account.entities.CandyShop;
import md.bass.account.sevices.CandyShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class CandyShopController {
    @Autowired
    CandyShopService candyShopService;

    @RequestMapping(value = "/candyshop" ,method = RequestMethod.GET)
    Iterable<CandyShop> getAllCandyShop(){
        return candyShopService.findAllCandyShop();
    }
    @RequestMapping(value = "/candyshop" ,method = RequestMethod.POST)
    void saveCandyShop(@RequestBody CandyShop candyShop){
        candyShopService.saveCandyShop(candyShop);
    }
    @RequestMapping(value = "/candyshop/{id}" ,method = RequestMethod.DELETE)
    void deleteCandyShop(@PathVariable Integer id){candyShopService.deleteCandyShop(id);}


}
