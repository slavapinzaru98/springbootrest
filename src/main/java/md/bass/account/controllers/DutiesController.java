package md.bass.account.controllers;

import md.bass.account.entities.Duties;
import md.bass.account.sevices.DutiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DutiesController {
    @Autowired
    DutiesService dutiesService;
    @RequestMapping(value = "/duties" ,method = RequestMethod.GET)
    Iterable<Duties> findAllDuties(){return dutiesService.findDuties();}
    @RequestMapping(value = "/duties" ,method = RequestMethod.POST)
    void saveDuties(@RequestBody Duties duties){ dutiesService.saveDuties(duties);}
    @RequestMapping(value = "/duties/{id}" ,method = RequestMethod.DELETE)
    void deleteDuties(@PathVariable Integer id){ dutiesService.deleteDuties(id);}

}
