package md.bass.account.controllers;

import md.bass.account.entities.Account;
import md.bass.account.sevices.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AccountController {
    @Autowired
    AccountService accountService;

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public Iterable<Account> getAllAccount() {
        return accountService.findAccount();
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public @ResponseBody
    void saveAccount(@RequestBody Account account) {
        accountService.saveAccount(account);
    }

    @RequestMapping(value = "/account/{id}", method = RequestMethod.DELETE)
    public void deleteAccount(@PathVariable Integer id) {
        accountService.deleteAccount(id);
    }

    @RequestMapping(value = "/account", method = RequestMethod.PUT)
    public @ResponseBody
    void updateAccount(Account account) {
        accountService.updateAccount(account);
    }

    @RequestMapping(value = "/account/{id}", method = RequestMethod.GET)
    public Optional<Account> getCertainAccount(@PathVariable Integer id) {
        return accountService.findCertainAccount(id);
    }

}
