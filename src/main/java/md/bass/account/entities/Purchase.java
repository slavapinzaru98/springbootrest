package md.bass.account.entities;

import javax.persistence.*;

@Entity
@Table(name = "purchase")
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    @Column(name = "purchased_product")
    private String purchasedProduct;
    @Column(name = "date_of_purchase")
    private String dateOfPurchase;
    @Column(name = "num_of_products_purchased")
    private String quantityBought;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "account_id" ,referencedColumnName = "id")
    public Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPurchasedProduct() {
        return purchasedProduct;
    }

    public void setPurchasedProduct(String purchasedProduct) {
        this.purchasedProduct = purchasedProduct;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getQuantityBought() {
        return quantityBought;
    }

    public void setQuantityBought(String quantityBought) {
        this.quantityBought = quantityBought;
    }
}
