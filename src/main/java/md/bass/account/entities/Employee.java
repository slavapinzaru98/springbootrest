package md.bass.account.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Basic
    @Column(name = "kind_of_employee")
    private String kind_of_employee;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE )
    @JoinTable(
            name = "employee_duties",
            joinColumns = {@JoinColumn(name = "id_employee")},
            inverseJoinColumns = {@JoinColumn(name = "id_duties")}
    )
    private Set<Duties> dutiesSet = new HashSet<>();

    public Set<Duties> getDutiesSet() { return dutiesSet; }
    public void setDutiesSet(Set<Duties> dutiesSet) { this.dutiesSet = dutiesSet; }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getKind_of_employee() {
        return kind_of_employee;
    }
    public void setKind_of_employee(String kind_of_employee) {
        this.kind_of_employee = kind_of_employee;
    }
}
