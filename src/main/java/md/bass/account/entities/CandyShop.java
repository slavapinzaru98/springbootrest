package md.bass.account.entities;


import javax.persistence.*;

@Entity
@Table(name = "candy_shop")
public  class  CandyShop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Basic
    @Column(name = "chocolate")
    private String chocolate;
    @Basic
    @Column(name = "jellies")
    private String jellies;
    @Basic
    @Column(name = "lollipops")
    private String lollipops;
    @Basic
    @Column(name = "gums")
    private String gums;

    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }
    public String getChocolate() { return chocolate; }
    public void setChocolate(String chocolate) { this.chocolate = chocolate; }
    public String getJellies() { return jellies; }
    public void setJellies(String jellies) { this.jellies = jellies; }
    public String getLollipops() { return lollipops; }
    public void setLollipops(String lollipops) { this.lollipops = lollipops; }
    public String getGums() { return gums; }
    public void setGums(String gums) { this.gums = gums; }
}
