package md.bass.account.entities;

import javax.persistence.*;

@Entity
@Table(name ="account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Column(name = "username")
    private String username;

    @Basic
    @Column(name = "password")
    private String password;

    @Basic
    @Column(name = "deposit")
    private  float deposit;


    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "candy_shop_id" , referencedColumnName = "id")
    private CandyShop candy_shop_id;

    public CandyShop getCandy_shop_id() { return candy_shop_id; }
    public void setCandy_shop_id(CandyShop candy_shop_id) { this.candy_shop_id = candy_shop_id; }
    public Integer getId() { return id; }
    public void setId(Integer id) { this.id = id; }
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }
    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }
    public float getDeposit() { return deposit; }
    public void setDeposit(float deposit) { this.deposit = deposit; }

}
