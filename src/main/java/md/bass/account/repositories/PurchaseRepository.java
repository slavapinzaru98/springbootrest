package md.bass.account.repositories;

import md.bass.account.entities.Purchase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends CrudRepository <Purchase, Integer> {
}
