package md.bass.account.repositories;

import md.bass.account.entities.CandyShop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandyShopRepository extends CrudRepository<CandyShop , Integer> {
}
