package md.bass.account.repositories;

import md.bass.account.entities.Duties;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DutiesRepository extends CrudRepository<Duties , Integer> {
}
