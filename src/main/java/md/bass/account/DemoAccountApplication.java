package md.bass.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"md.bass.account"})
@EnableJpaRepositories("md.bass.account.repositories")
@EntityScan("md.bass.account.entities")
public class DemoAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoAccountApplication.class, args);
    }

}
